(function(){
	var app = angular.module('smallBlog', []);

	app.directive('postsList', function(){
		return {
			restrict: "E",
			templateUrl: "templates/posts-list-template.html",
			controller: function(){
				var self = this;

				self.post = {};
				self.posts = [];
				self.addPostFormVisible = false;

				self.toggleForm = function(){
					self.addPostFormVisible = !self.addPostFormVisible;
				}
				self.clearForm = function(){
					self.post = {};
				}
				self.savePost = function(post){
					post.dateCreated = new Date();
					self.posts.push(post);
					self.clearForm();
					self.addPostFormVisible = false;
				}
			},
			controllerAs: 'postCtrl'
		};
	});
})();